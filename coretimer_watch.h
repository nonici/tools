/* 
 * File: Timers.h / Timers.c  
 * Author: Flitch
 * Comments: Software Timers using Coretimer up to:
 * 4 294 967 296 ms
 * 4 294 967 seconds
 *    71 582 minutes
 *     1 193 hours
 *        49  days
 *         7 weeks
 * Revision history: V1_I1
 */

#ifndef CORETIMER_WATCH_H
#define	CORETIMER_WATCH_H

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "peripheral/coretimer/plib_coretimer.h"

typedef struct {
    uint32_t startingTime;
    //uint32_t
} watch_timer_t;


void CTW_Initialize(void);
void CTW_Handler(void);
void CTW_Tick(void);
void CTW_StartRestart(watch_timer_t *tmr);

bool CTW_IsWatchRunningForMiliseconds(watch_timer_t *tmr, uint32_t timeMs);
bool CTW_IsWatchRunningForSeconds(watch_timer_t *tmr, uint32_t timeSeconds);
bool CTW_IsWatchRunningForMinutes(watch_timer_t *tmr, uint32_t timeMinutes);
bool CTW_IsWatchRunningForHours(watch_timer_t *tmr, uint32_t timeHours);
bool CTW_IsWatchRunningForDays(watch_timer_t *tmr, uint32_t timeDays);
bool CTW_IsWatchRunningForWeeks(watch_timer_t *tmr, uint32_t timeWeeks);

#endif	/* CORETIMER_WATCH_H */

