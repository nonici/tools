/* ************************************************************************** */
/*
  @File Name
    Linear.h
 
  @Author
    Flitch

  @Summary
    u8 & u32 linear buffer.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef TOOLS_LINEAR_H    /* Guard against multiple inclusion */
#define TOOLS_LINEAR_H

// *****************************************************************************

#include <stdint.h>
#include <stdbool.h>

// *****************************************************************************

typedef struct {
    uint32_t size;
    uint32_t head;
    uint32_t tail;
    uint8_t* element;
} linearu8_t;

// *****************************************************************************

//Variables

// *****************************************************************************

static inline void LinearU8Initialize(linearu8_t* buffer, uint8_t* buffMem, uint32_t size) {
    buffer->size = size;
    buffer->head = 1;
    buffer->tail = 1;
    buffer->element = (uint8_t*) buffMem;
    buffer->element[0] = 0;
}

static inline uint8_t LinearU8ReadChar(linearu8_t* buffer) {
    uint8_t data = buffer->element[buffer->tail];
    buffer->tail = (buffer->tail + 1);
    if (buffer->tail < (buffer->size - 1)) {
        ++buffer->tail;
    }
    return (data);
}

static inline void LinearU8WriteChar(linearu8_t* buffer, uint8_t data) {
    buffer->element[buffer->head] = data;
    buffer->head = (buffer->head + 1);
    if (buffer->head < (buffer->size - 1)) {

        ++buffer->head;
    }
}

static inline uint32_t LinearU8SniffForMatch(linearu8_t* buffer, uint8_t match) {
    uint8_t data = 0;
    uint32_t snout = buffer->tail;
    while (snout < buffer->size) {
        data = buffer->element[snout];
        snout = snout + 1;
        if (data == match) {

            return (snout);
        }
    }
    return (0);
}

static inline uint32_t LinearU8DataInside(linearu8_t* buffer) {
    uint32_t head = buffer->head;
    uint32_t tail = buffer->tail;
    return (head - tail);
}

// *****************************************************************************

#endif /*  TOOLS_LINEAR_H */

/* *****************************************************************************
 End of File
 */
