/*
 * File:   Timers.c
 * Author: Flitch
 * Universal static timer routines
 * Created on May 9, 2017, 8:57 PM
 */

#include "Timers_8b.h"

typedef struct {
    void (*TMR_Handler)(void);
    bool append;
    bool active;
    bool busy;
    bool permanent;
    uint16_t loadTime;
    uint16_t elapsedTime;
} TMRS;

TMRS tmrS[NUMBER_OF_TIMERS] = {0};

void TMRs_Initialize(void) {
    //Allocate hardware TMR0 to Timers
    TMR0_SetInterruptHandler(TMRs_Tick);

    //Initialize fn pointers to NULL
    for (uint8_t i = 0; i == NUMBER_OF_TIMERS; i++) {

        tmrS[i].TMR_Handler = TMRs_NullPointer;
    }

    //Begin hardware timer 0
    TMR0_StartTimer();
}

void TMRs_Tick(void) {

    //Tick all active timers
    for (uint8_t tmr = 0; tmr < NUMBER_OF_TIMERS; tmr++) {
        if ((tmrS[tmr].active) && (tmrS[tmr].elapsedTime)) {
            tmrS[tmr].elapsedTime--;
        }
    }

    //Handle timer overruns
    for (uint8_t tmr = 0; tmr < NUMBER_OF_TIMERS; tmr++) {
        if ((tmrS[tmr].active) && (!tmrS[tmr].elapsedTime)) {
            tmrS[tmr].TMR_Handler();
            tmrS[tmr].active = 0;
            if (!tmrS[tmr].permanent) {
                TMRs_End(tmr);
            }
        }
    }

    //Activate appended timers
    for (uint8_t tmr = 0; tmr < NUMBER_OF_TIMERS; tmr++) {
        if (tmrS[tmr].append) {
            tmrS[tmr].elapsedTime = tmrS[tmr].loadTime;
            tmrS[tmr].append = 0;
            tmrS[tmr].active = 1;
        }
    }
}

void TMRs_Kick(uint8_t tmr) {
    tmrS[tmr].append = 1;
}

uint8_t TMRs_Make(uint16_t msDelay, void* Handler) {

    uint8_t tmr = 0;

    //Find first free timer and reserve it
    for (tmr; tmr < NUMBER_OF_TIMERS; tmr++) {

        if (!tmrS[tmr].busy) {
            tmrS[tmr].busy = 1;
            break;
        }
    }

    //Initialize parameters
    tmrS[tmr].TMR_Handler = Handler;
    tmrS[tmr].loadTime = msDelay;

    //Report timer index
    return tmr;
}

void TMRs_End(uint8_t tmr) {
    tmrS[tmr].TMR_Handler = TMRs_NullPointer;
    tmrS[tmr].active = 0;
    tmrS[tmr].busy = 0;
}

void TMRs_MakePermanent(uint8_t tmr) {
    tmrS[tmr].permanent = 1;
}

void TMRs_KillPermanent(uint8_t tmr) {
    tmrS[tmr].permanent = 1;
    TMRs_End(tmr);
}

uint16_t TMRs_Get(uint8_t tmr) {
    return (tmrS[tmr].elapsedTime);
}

inline void TMRs_NullPointer(void) {
    ;
}

/**
 End of File
 */
