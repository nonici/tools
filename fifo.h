/* ************************************************************************** */
/*
  @File Name
    fifo.h
 
  @Author
    Flitch

  @Summary
    Generic FIFO buffer.

  @Description
    Can be used with simple and complex data structures.
 */
/* ************************************************************************** */

#ifndef FIFO_H    /* Guard against multiple inclusion */
#define FIFO_H

// *****************************************************************************

#include <stdint.h>

// *****************************************************************************

#define fifo_t(T, NAME) \
  typedef struct { \
    uint32_t size; \
    uint32_t head; \
    uint32_t tail; \
    T* elems; \
    } NAME

#define FifoInitialize(BUF, S, T, BUFMEM) do{ \
    BUF->size = S/sizeof(T); \
    BUF->head = 0; \
    BUF->tail = 0; \
    BUF->elems = (T*) (BUFMEM); \
    } while (0);

#define FifoWrite(BUF, ELEM) do{\
    BUF->elems[BUF->head]=ELEM;\
    BUF->head=(BUF->head+1)%BUF->size;\
}while (0)

#define FifoRead(BUF, ELEM)do{\
    ELEM=BUF->elems[BUF->tail];\
    BUF->tail=(BUF->tail+1)%BUF->size;\
}while (0)

#define FifoFlush(BUF)do{\
    BUF->start = 0; \
    BUF->end = 0; \
}while (0)

#define FifoDataInside(BUFF,DATA) do{\
    if (BUF->head < BUF->tail) {\
    DATA = BUF->head + BUF->size - BUF->tail;\
    }else{\
    DATA = BUF->head - BUF->tail;}\
}while (0)

// *****************************************************************************

fifo_t(uint8_t, fifou8_t);
fifo_t(uint16_t, fifou16_t);
fifo_t(uint32_t, fifou32_t);

// *****************************************************************************

#endif /* FIFO_H */

/* *****************************************************************************
 End of File
 */
