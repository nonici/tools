/* ************************************************************************** */
/*
  @File Name
    feeder_dictionary.h
 
  @Author
    Flitch

  @Summary
    Object dictionary for feeders.

  @Description
    This file contains definitions of all objects required by feeders operating on can bus.
    It is customized striped down version of CAN Open object dictionary.
 */
/* ************************************************************************** */

#ifndef FEEDER_DICTIONARY_H    /* Guard against multiple inclusion */
#define FEEDER_DICTIONARY_H

// *****************************************************************************

#include <stdint.h>
#include <stdbool.h>

// *****************************************************************************

/* 
 * Feeder status one message, 8bits <ABCDEFGH>
 * Constructed as <status> + <lane flag> : <ABCD + EFGH>
 * Status:      ABCD    fdr_status_report_opcode_t
 * Lane flag:   EFGH    fdr_lane_flags_t 
 * 
 * */

#define MASK_FEEDER_STATUS_OPCODE       0b11110000
#define MASK_FEEDER_STATUS_LANE_FLAG    0b00001111

#define MASK_FEEDER_COMMAND             0b11000000
#define MASK_FEEDER_LANE_NUMBER         0b00110000
#define MASK_FEEDER_PITCH               0b00001111

#define SLOT_ID_OFFSET_FROM_PHISICAL_ADDRESS 20

typedef enum {
    BF_FSR_NO_REPORT = 0x00,
    BF_FSR_HELLO_LOW_PWR = 0x10,
    BF_FSR_HELLO_HIGH_PWR = 0x20,
    BF_FSR_READY = 0x30,
    BF_FSR_FEEDING = 0x40,
    BF_FSR_ERROR = 0x50,
    BF_FSR_TIMEOUT = 0x60,
    BF_FSR_COMPLETED = 0x70,
    BF_FSR_RESERVED = 0x80,
} fdr_status_report_opcode_t;

typedef enum {
    BF_LANE_NO_FLAG = 0x00,
    BF_LANE_FLAG_1 = 0x01,
    BF_LANE_FLAG_2 = 0x02,
    BF_LANE_FLAG_3 = 0x04,
    BF_LANE_FLAG_4 = 0x08,
} fdr_lane_flags_bf_t;

/* 
 * Feeder command one message, 8bits <ABCDEFGH>
 * Constructed as <command> + <lane number> + <pitch/settings> : <ABCD + EFGH>
 * Command:         AB      feeder_cmd_t
 * Lane number:     CD      fdr_lane_number_t
 * Pitch/Settings:  EFGH    fdr_lane_pitch_t
 * 
 * */

typedef enum {
    BF_FDR_CMD_CUSTOM = 0x00,
    BF_FDR_CMD_ADVANCE = 0x40,
    BF_FDR_CMD_RETRACT = 0x80,
    BF_FDR_CMD_PEEL = 0xC0,
} feeder_cmd_bf_t;

typedef enum {
    BF_LANE_NO_1 = 0x00,
    BF_LANE_NO_2 = 0x10,
    BF_LANE_NO_3 = 0x20,
    BF_LANE_NO_4 = 0x30,
} fdr_lane_number_bf_t;

typedef enum {
    LANE_1 = 0,
    LANE_2 = 1,
    LANE_3 = 2,
    LANE_4 = 3,
} fdr_lane_id_t;

typedef enum {
    BF_PITCH_DEFAULT = 0x00,
    BF_PITCH_MM_02 = 0x01,
    BF_PITCH_MM_04 = 0x02,
    BF_PITCH_MM_08 = 0x03,
    BF_PITCH_MM_12 = 0x04,
    BF_PITCH_MM_16 = 0x05,
    BF_PITCH_MM_20 = 0x06,
    BF_PITCH_MM_24 = 0x07,
    BF_PITCH_MM_28 = 0x08,
    BF_PITCH_MM_32 = 0x09,
    BF_PITCH_MM_36 = 0x0A,
    BF_PITCH_MM_40 = 0x0B,
    BF_PITCH_MM_44 = 0x0C,
    BF_PITCH_MM_48 = 0x0D,
    BF_PITCH_MM_52 = 0x0E,
    BF_PITCH_MM_56 = 0x0F,

} fdr_pitch_bf_t;

enum {
    COMMAND_NO_COMMAND,
    COMMAND_RECEIVED,
    COMMAND_INVALID,
    COMMAND_COMPILED,
    COMMAND_DISPATCHED,
    COMMAND_ACKNOWLEDGED,
    COMMAND_EXECUTING,
    COMMAND_ERROR,
    COMMAND_DONE,
    COMMAND_SLOT_NOT_FOUND,
    COMMAND_FEEDER_NOT_FOUND,
} feeder_command_status_t;

// *****************************************************************************

//Variables

typedef struct {
    char letter;
    uint8_t slotNumber;
    uint8_t laneNumber;
    uint8_t pitchNumber;
    feeder_cmd_bf_t cmdBf;
    fdr_lane_id_t laneBf;
    fdr_pitch_bf_t pitchBf;
    uint8_t compiledCommand;
    uint8_t compiledReport;
    //feeder_command_status_t status;
} feeder_command_t;

typedef union {

    struct {
        bool keypadKeyF1 : 1;
        bool keypadKeyF2 : 1;
        bool keypadKeyF3 : 1;
        bool keypadKeyF4 : 1;
        bool keypadSwitchP2 : 1;
        bool keypadSwitchFw : 1;
        bool keypadSwitchLoop : 1;
        bool keypadPressent : 1;
    } bits;
    uint8_t map;
} keypad_key_map_bf_t;


// *****************************************************************************

// *****************************************************************************

static inline uint8_t FeederComposeCommand(feeder_cmd_bf_t command, fdr_lane_id_t lane, fdr_pitch_bf_t pitch) {
    uint8_t composedCommand = (uint8_t) command;
    if (lane == LANE_1)composedCommand |= (uint8_t) BF_LANE_NO_1;
    if (lane == LANE_2)composedCommand |= (uint8_t) BF_LANE_NO_2;
    if (lane == LANE_3)composedCommand |= (uint8_t) BF_LANE_NO_3;
    if (lane == LANE_4)composedCommand |= (uint8_t) BF_LANE_NO_4;
    composedCommand |= (uint8_t) pitch;
    return (composedCommand);
}

static inline feeder_command_t FeederComposeCommandFromKeymap(uint8_t map) {

    keypad_key_map_bf_t keyMap = (keypad_key_map_bf_t) map;
    feeder_command_t result;

    //Get lane
    if (keyMap.bits.keypadKeyF1)result.laneNumber = LANE_1;
    if (keyMap.bits.keypadKeyF2)result.laneNumber = LANE_2;
    if (keyMap.bits.keypadKeyF3)result.laneNumber = LANE_3;
    if (keyMap.bits.keypadKeyF4)result.laneNumber = LANE_4;

    //Get pitch
    result.pitchBf = BF_PITCH_MM_04;

    if (keyMap.bits.keypadSwitchP2)result.pitchBf = BF_PITCH_MM_02;
    if (keyMap.bits.keypadSwitchLoop)result.pitchBf = BF_PITCH_MM_16;

    //Get feeder command

    if (keyMap.bits.keypadSwitchFw) {
        //Feed
        result.cmdBf = BF_FDR_CMD_ADVANCE;
    } else {
        //Retract
        result.cmdBf = BF_FDR_CMD_RETRACT;
    }

    result.compiledCommand = FeederComposeCommand(result.cmdBf, result.laneNumber, result.pitchBf);
    return (result);
}

static inline fdr_status_report_opcode_t FeederGetStatusFlagsFromStatusWord(uint8_t word) {
    return (word & MASK_FEEDER_STATUS_OPCODE);
}

static inline fdr_lane_flags_bf_t FeederGetLaneFlagsFromStatusWord(uint8_t word) {
    return (word & MASK_FEEDER_STATUS_LANE_FLAG);
}

// *****************************************************************************

#endif /* FEEDER_DICTIONARY_H */

/* *****************************************************************************
 End of File
 */
