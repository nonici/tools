/* ************************************************************************** */
/*
  @File Name
    ring.h
 
  @Author
    Flitch

  @Summary
    u8 & u32 ringbuffer.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef RING_H    /* Guard against multiple inclusion */
#define RING_H

// *****************************************************************************

#include <stdint.h>
#include <stdbool.h>

// *****************************************************************************

typedef struct {
    uint32_t size;
    uint32_t head;
    uint32_t tail;
    uint8_t* element;
} ringu8_t;

typedef struct {
    uint32_t size;
    uint32_t head;
    uint32_t tail;
    uint32_t* element;
} ringu32_t;

// *****************************************************************************

//Variables

// *****************************************************************************

static inline void RingU8Initialize(ringu8_t* buffer, uint8_t* buffMem, uint32_t size) {
    buffer->size = size;
    buffer->head = 0;
    buffer->tail = 0;
    buffer->element = (uint8_t*) buffMem;
}

static inline void RingU32Initialize(ringu32_t* buffer, uint32_t* buffMem, uint32_t size) {
    buffer->size = size;
    buffer->head = 0;
    buffer->tail = 0;
    buffer->element = (uint32_t*) buffMem;
}

static inline uint8_t RingU8Read(ringu8_t* buffer) {
    uint8_t data = buffer->element[buffer->tail];
    buffer->tail = (buffer->tail + 1) % buffer->size;
    return (data);
}

static inline uint32_t RingU32Read(ringu32_t* buffer) {
    uint32_t data = buffer->element[buffer->tail];
    buffer->tail = (buffer->tail + 1) % buffer->size;
    return (data);
}

static inline void RingU8Write(ringu8_t* buffer, uint8_t data) {
    buffer->element[buffer->head] = data;
    buffer->head = (buffer->head + 1) % buffer->size;
}

static inline void RingU32Write(ringu32_t* buffer, uint32_t data) {
    buffer->element[buffer->head] = data;
    buffer->head = (buffer->head + 1) % buffer->size;
}

static inline void RingU8AppendString(ringu8_t* buffer, char* dataBuff) {
    uint8_t i = 0;
    while (dataBuff[i]) {
        buffer->element[buffer->head] = (uint8_t) dataBuff[i++];
        buffer->head = (buffer->head + 1) % buffer->size;
    }
}

static inline void RingU8AppendArray(ringu8_t* buffer, uint8_t* dataBuff, uint8_t length) {
    uint8_t i = 0;
    while (i < length) {

        buffer->element[buffer->head] = dataBuff[i];
        buffer->head = (buffer->head + 1) % buffer->size;
        i++;
    }
}

static inline void RingU32AppendArray(ringu32_t* buffer, uint32_t* dataBuff, uint32_t length) {
    uint32_t i = 0;
    while (i < length) {

        buffer->element[buffer->head] = dataBuff[i];
        buffer->head = (buffer->head + 1) % buffer->size;
        i++;
    }
}

static inline bool RingU8SniffForMatch(ringu8_t* buffer, uint8_t match) {
    uint8_t data = 0;
    uint32_t snout = buffer->tail;
    while (buffer->head != snout) {
        data = buffer->element[snout];
        snout = (snout + 1) % buffer->size;
        if (data == match) {

            return (true);
        }
    }
    return (false);
}

static inline bool RingU32SniffForMatch(ringu32_t* buffer, uint32_t match) {
    uint32_t data = 0;
    uint32_t snout = buffer->tail;
    while (buffer->head != snout) {
        data = buffer->element[snout];
        snout = (snout + 1) % buffer->size;
        if (data == match) {

            return (true);
        }
    }
    return (false);
}

static inline uint32_t RingU8DataInside(ringu8_t* buffer) {
    uint32_t head = buffer->head;
    uint32_t tail = buffer->tail;
    if (head < tail) {

        head += buffer ->size;
    }
    return (head - tail);
}

static inline uint32_t RingU32DataInside(ringu32_t* buffer) {
    uint32_t head = buffer->head;
    uint32_t tail = buffer->tail;
    if (head < tail) {
        head += buffer ->size;
    }
    return (head - tail);
}

static inline uint32_t RingU8FreeSpace(ringu8_t* buffer) {
    uint32_t size = buffer ->size;
    uint32_t dataInside = RingU8DataInside(buffer);
    return (size - dataInside);
}

static inline uint32_t RingU32FreeSpace(ringu32_t* buffer) {
    uint32_t size = buffer ->size;
    uint32_t dataInside = RingU32DataInside(buffer);
    return (size - dataInside);
}
// *****************************************************************************

#endif /* RING_H */

/* *****************************************************************************
 End of File
 */
