/* ************************************************************************** */
/*
  @File Name
    ms_timer.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef MS_TIMER_H    /* Guard against multiple inclusion */
#define MS_TIMER_H

// *****************************************************************************

#include <stdint.h>
#include <stdbool.h>

// *****************************************************************************

typedef struct {
    volatile uint32_t start;
    volatile uint32_t time;
    volatile bool isRuning;
    //volatile bool isPeriodic;
} ms_timer_t;

// *****************************************************************************

//functions
void MsTimerTick(void);
void MsTimerDelayMs(ms_timer_t *timer, uint32_t time);
void MsTimerWaitMs(uint32_t time);
bool MsTimerIsTimerActive(ms_timer_t *timer);

// *****************************************************************************

#endif /* MS_TIMER_H */

/* *****************************************************************************
 End of File
 */
