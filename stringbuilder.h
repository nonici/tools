/* ************************************************************************** */
/*
  @File Name
    stringbuilder.h
 
  @Author
    Flitch

  @Summary
    stringbuilder tool

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef STRINGBUILDER_H    /* Guard against multiple inclusion */
#define STRINGBUILDER_H

// *****************************************************************************
#include "printf.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// *****************************************************************************

//#define NL_CHAR         0x0A            //New line
//#define CR_CHAR         0x0D            //Carriage Return
//#define COMMA_CHAR      0x2C            //Comma character
//static const char CRNL_CHARS[] = "/r/n";
//static const char HEX_CHARS[] = "0123456789ABCDEF";

// *****************************************************************************

typedef struct {
    char * start; // points to the start of the buffer
    char * end; // points to the byte after the end of the buffer
    char * current; // points to the current byte
}
stringbuilder_t;

// *****************************************************************************

// Initializes an existing stringbuilder_t using the specified buffer.
// First character in the buffer is immediately set to '\0',
// builder capacity (max length without '\0') is set to (bufferSize-1).
// (i.e. bufferSize must include the null-terminating character).

static inline bool StringbuilderIsNotFull(const stringbuilder_t *builder) {
    return (bool)(builder->current < builder->end);
}

static inline uint32_t StringbuilderRemainingCapacity(const stringbuilder_t *builder) {
    return (uint32_t) (builder->end - builder->current);
}

static inline void StringbuilderResetBuilder(stringbuilder_t *builder) {
    builder->current = builder->start;
    *builder->current = '\0';
}

static inline void StringbuilderClearBuilder(stringbuilder_t *builder) {
    builder->current = builder->start;
    *builder->current = '\0';
}

static inline uint32_t StringbuilderTotalCapacity(const stringbuilder_t *builder) {
    return (uint32_t) (builder->end - builder->start);
}

static inline uint32_t StringbuilderStringLength(const stringbuilder_t *builder) {
    return (uint32_t) (builder->current - builder->start);
}

static inline void StringbuilderIntialize(stringbuilder_t *builder, char * buffer, uint32_t bufferSize) {
    builder->start = buffer;
    builder->end = buffer + bufferSize - 1;
    builder->current = buffer;

    *builder->end = '\0';
    *builder->current = '\0';
}

static inline stringbuilder_t StringbuilderCreateBuilder(char * buffer, uint32_t bufferSize) {
    stringbuilder_t sb;
    StringbuilderIntialize(&sb, buffer, bufferSize);
    return sb;
}

static inline void StringbuilderAppendString(stringbuilder_t *builder, const char *msg) {
    while (StringbuilderIsNotFull(builder) && *msg != 0) {
        *builder->current++ = *msg++;
    }

    *builder->current = '\0';

}

static inline void StringbuilderAppendChar(stringbuilder_t *builder, char character) {
    if (StringbuilderIsNotFull(builder) && (*builder->current = character) != 0) {
        builder->current++;
    }

    *builder->current = '\0';
}

static inline void StringbuilderAppendU32(stringbuilder_t *builder, uint32_t num) {
    char numArr[20];
    utoa(numArr, num, 10);
    StringbuilderAppendString(builder, numArr);
}

static inline void StringbuilderAppendI32(stringbuilder_t *builder, int32_t num) {
    char numArr[20];
    itoa(numArr, num, 10);
    StringbuilderAppendString(builder, numArr);
}

static inline void StringbuilderAppendFloat(stringbuilder_t *builder, float num) {
    char numArr[20];
    tiny_snprintf(numArr, 19, "%.6f", (double) num);
    StringbuilderAppendString(builder, numArr);
}

static inline void StringbuilderAppendDouble(stringbuilder_t *builder, double num) {
    char numArr[20];
    tiny_snprintf(numArr, 19, "%.4f", (double) num);
    StringbuilderAppendString(builder, numArr);
}

static inline void StringbuilderAppendCrLf(stringbuilder_t *builder) {
    StringbuilderAppendString(builder, "\r\n");
}

//inline char * string_get_data_pointer(const stringbuilder_t *builder) {
//    return builder->start;
//}

//static inline void string_reset(stringbuilder_t *builder);
//static inline void string_clear(stringbuilder_t *builder);

//static inline uint32_t string_capacity(const stringbuilder_t *builder);
//static inline uint32_t string_length(const stringbuilder_t *builder);
//static inline void StringbuilderStringLength(void);

//static inline void StringbuilderAppendChar(void);
//static inline void StringbuilderAppendString(void);
//static inline void StringbuilderAppendNumber(void);
//static inline void StringbuilderAppendHex(void);
//static inline void StringbuilderAppendCrNl(void);
//static inline void StringbuilderAppendTimestamp(void);


// *****************************************************************************

#endif /* STRINGBUILDER_H */

/* *****************************************************************************
 End of File
 */
