/* 
 * File: Timers.h / Timers.c  
 * Author: Flitch
 * Comments: Soft Timers up to 65536 miliseconds using TMR_0 
 * Revision history: V1_I1
 */

//Timer is modified to use 1.007616 s instead of 1 ms
//This results in 30 minutes being 1786.39 interrupts, or error of 27 s/h
//Timeouts being:
//T0 = 1i      / 1s     / 0m
//T1 = 1786i   / 1800s  / 30m
//T2 = 3.573i  / 3600s  / 60m
//T3 = 5.359i  / 5400s  / 90m

//Test values
//#define TIMEOUT_SETTINGS_MINUTES_00 0
//#define TIMEOUT_SETTINGS_MINUTES_01 1
//#define TIMEOUT_SETTINGS_MINUTES_02 2
//#define TIMEOUT_SETTINGS_MINUTES_03 3
//#define TIMEOUT_SETTINGS_RELAY_MINUTES 1
//

#define TIMEOUT_SETTINGS_MINUTES_00 0
#define TIMEOUT_SETTINGS_MINUTES_01 30
#define TIMEOUT_SETTINGS_MINUTES_02 60
#define TIMEOUT_SETTINGS_MINUTES_03 90
#define TIMEOUT_SETTINGS_RELAY_MINUTES 60

#define TIMEOUT_REAL_PERIOD 1.007716

#define TIMEOUT_TIME_00 ((uint16_t)(TIMEOUT_SETTINGS_MINUTES_00*60/TIMEOUT_REAL_PERIOD)+1)
#define TIMEOUT_TIME_01 ((uint16_t)(TIMEOUT_SETTINGS_MINUTES_01*60/TIMEOUT_REAL_PERIOD))
#define TIMEOUT_TIME_02 ((uint16_t)(TIMEOUT_SETTINGS_MINUTES_02*60/TIMEOUT_REAL_PERIOD))
#define TIMEOUT_TIME_03 ((uint16_t)(TIMEOUT_SETTINGS_MINUTES_03*60/TIMEOUT_REAL_PERIOD))
#define TIMEOUT_TIME_RELAY ((uint16_t)(TIMEOUT_SETTINGS_RELAY_MINUTES*60/TIMEOUT_REAL_PERIOD))

#ifndef TIMERS_H
#define	TIMERS_H
//#include "mcc_generated_files/mcc.h"

//Set required number of timers
#define     NUMBER_OF_TIMERS        10

void TMRs_Initialize(void);
//Initialization routine
//Uses TMR0
//Call after initialization of timer 0

void TMRs_Tick(void);
//Ticks all soft timers
//Handles overflows

void TMRs_Kick(uint8_t tmr);
//Start or restart timer

uint8_t TMRs_Make(uint16_t msDelay, void* Handler);
//Create new timer
//Should be called at Initialization

void TMRs_End(uint8_t tmr);
//Stop selected timer

uint16_t TMRs_Get(uint8_t tmr);
//Get current time

//Makes timer permanent
void TMRs_MakePermanent(uint8_t tmr);
/*Permanent timers wont get killed on overflow,however it can still be killed manualy using TMRs_KillPermanent*/

//Destroys permanent timer
void TMRs_KillPermanent(uint8_t tmr);

//Null pointer
inline void TMRs_NullPointer(void);

#endif	/* TIMERS_H */

