/* 
 * File: Timers.h / Timers.c  
 * Author: Flitch
 * Comments: Software Timers using Coretimer up to:
 * 4 294 967 296 ms
 * 4 294 967 seconds
 *    71 582 minutes
 *     1 193 hours
 *        49  days
 *         7 weeks
 * Revision history: V1_I1
 */
#include "coretimer_watch.h"

uint32_t timerMsPeriods = 0;
uint32_t timerTickCounter = 0;
uint32_t timer1MsCountConstant;

void CTW_Initialize(void) {
    CORETIMER_Start();
    timerTickCounter = _CP0_GET_COUNT();
    timer1MsCountConstant = (CORETIMER_FrequencyGet() / 1000U);
}

void CTW_StartRestart(watch_timer_t *tmr) {
    tmr->startingTime = timerMsPeriods;
}

void CTW_Handler(void) {
    //Pooled from main when using non-interrupt mode

    if (_CP0_GET_COUNT() - timerTickCounter > timer1MsCountConstant) {
        timerMsPeriods++;
        //Start from here
        timerTickCounter = _CP0_GET_COUNT();
    }
}

void CTW_Tick(void) {
    //Called from 1 ms interrupt when using interrupt mode
    timerMsPeriods++;
}

bool CTW_IsWatchRunningForMiliseconds(watch_timer_t *tmr, uint32_t timeMs) {
    // Calculate the end count for the given delay in miliseconds
    uint32_t endTime = tmr->startingTime + timeMs;

    if (timerMsPeriods >= endTime) {
        return (1);
    } else {
        return (0);
    }
}

bool CTW_IsWatchRunningForSeconds(watch_timer_t *tmr, uint32_t timeSeconds) {
    uint32_t timeMs = timeSeconds * 1000;
    return (CTW_IsWatchRunningForMiliseconds(tmr, timeMs));
}

bool CTW_IsWatchRunningForMinutes(watch_timer_t *tmr, uint32_t timeMinutes) {
    uint32_t timeMs = timeMinutes * 1000 * 60;
    return (CTW_IsWatchRunningForMiliseconds(tmr, timeMs));
}

bool CTW_IsWatchRunningForHours(watch_timer_t *tmr, uint32_t timeHours) {
    uint32_t timeMs = timeHours * 1000 * 60 * 60;
    return (CTW_IsWatchRunningForMiliseconds(tmr, timeMs));
}

bool CTW_IsWatchRunningForDays(watch_timer_t *tmr, uint32_t timeDays) {
    uint32_t timeMs = timeDays * 1000 * 60 * 60 * 24;
    return (CTW_IsWatchRunningForMiliseconds(tmr, timeMs));
}

bool CTW_IsWatchRunningForWeeks(watch_timer_t *tmr, uint32_t timeWeeks) {
    uint32_t timeMs = timeWeeks * 1000 * 60 * 60 * 24 * 7;
    return (CTW_IsWatchRunningForMiliseconds(tmr, timeMs));
}
