// *****************************************************************************
/*
  @File Name
    ms_timer.h
 
  @Author
    Flitch

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
// *****************************************************************************

#include "ms_timer.h"

// *****************************************************************************

uint32_t internalTime = 0;

// *****************************************************************************


// *****************************************************************************

//Call this function from interrupt once every 1 ms

void MsTimerTick(void) {
    internalTime++;
}

void MsTimerDelayMs(ms_timer_t *timer, uint32_t time) {
    timer->start = internalTime;
    timer->time = time;
}

void MsTimerWaitMs(uint32_t time) {
    ms_timer_t tmrHere;
    MsTimerDelayMs(&tmrHere, time);
    while (MsTimerIsTimerActive(&tmrHere));
}

bool MsTimerIsTimerActive(ms_timer_t *timer) {
    return ((internalTime - timer->start) < timer->time);

    //    timer->isRuning = ((internalTime - timer->start) < timer->time);
    //    if (timer->isPeriodic) {
    //        //Auto reload timer
    //        MsTimerDelayMs(timer, timer->time);
    //        return (0);
    //    }
    //    return (timer->isRuning);
}

/* *****************************************************************************
 End of File
 */
