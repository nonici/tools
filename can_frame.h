/* ************************************************************************** */
/*
  @File Name
    can_frame.h
 
  @Author
    Flitch

  @Summary
    CAN frame for PNP.

  @Description
    This file contains definitions of CAN frame used on PNP.
    It is a striped down version of CANOpen.
 */
/* ************************************************************************** */

#include <stdint.h>

// *****************************************************************************
#ifndef CAN_FRAME_H    /* Guard against multiple inclusion */
#define CAN_FRAME_H

#define CAN_FUNCTION_CODE_MASK                  0x780
#define CAN_NODE_ID_MASK                        0x7F

//Defines NODE_ID for given slot (slot 0 = Node ID 20)
#define SLOT_ID_OFFSET_FROM_PHISICAL_ADDRESS    20

// *****************************************************************************



//#ifndef PLIB_CAN_COMMON_H

typedef enum {
    CAN_MSG_DATA_FRAME = 0,
    CAN_MSG_REMOTE_FRAME = 1,
} CAN_MSG_ATTRIBUTE;

//#endif

typedef struct {
    uint8_t idType;
    uint32_t cobId;
    uint32_t functionCode;
    uint8_t nodeId;
    uint8_t dlc;
    uint8_t data0;
    uint8_t data1;
    uint8_t data2;
    uint8_t data3;
    uint8_t data4;
    uint8_t data5;
    uint8_t data6;
    uint8_t data7;
    uint16_t timestamp;
    uint8_t fifoNum;
    CAN_MSG_ATTRIBUTE msgAttr;
} canbus_msg_t;

typedef enum {
    NMT = 0x000,
    SYNC = 0x080,
    EMERGENCY = 0x080,
    TIMESTAMP = 0x100,
    PDO1TX = 0x180,
    PDO1RX = 0x200,
    PDO2TX = 0x280,
    PDO2RX = 0x300,
    PDO3TX = 0x380,
    PDO3RX = 0x400,
    PDO4TX = 0x480,
    PDO4RX = 0x500,
    SDOTX = 0x580,
    SDORX = 0x600,
    NMTHB = 0x700,
} canbus_function_code_t;

typedef enum {
    STATE_STOPPED = 0x04,
    STATE_OPERATIONAL = 0x05,
    STATE_PREOPERATIONAL = 0x7F,
} hb_state_t;

// *****************************************************************************

static inline uint8_t CanGetNodeIdFromSlotAddress(uint8_t slotAddress) {
    return (slotAddress + SLOT_ID_OFFSET_FROM_PHISICAL_ADDRESS);
}

static inline uint8_t CanGetSlotAddressFromNodeId(uint8_t nodeId) {
    return (nodeId - SLOT_ID_OFFSET_FROM_PHISICAL_ADDRESS);
}

static inline void CanGetFunctionCodeAndNodeIdFromCobId(canbus_msg_t *msg) {
    msg->functionCode = msg->cobId & CAN_FUNCTION_CODE_MASK;
    msg->nodeId = msg->cobId & CAN_NODE_ID_MASK;
}

static inline void CanGetCobIdFromFunctionCodeAndNodeId(canbus_msg_t *msg) {
    msg->cobId = msg->functionCode | msg->nodeId;
}

// *****************************************************************************

/*
 * CAN OPEN Frame description:
 * 11 bit identifier
 * <FFFFNNNNNNNN>
 * 
 * FFFF = frame type / 4-bit
 * NNNNNNNN = node ID/ 8-bit
 * 
 * 
 * Communication object 	COB-ID(s) hex 
 * 
 * NMT node control         000 + NodeID 	
 * Sync                     080 	
 * Emergency                080 + NodeID 	
 * TimeStamp                100 	
 * 
 * PDO1RX                   180 + NodeID
 * PDO1TX                   200 + NodeID
 * PDO1RX                   280 + NodeID
 * PDO1TX                   300 + NodeID
 * PDO1RX                   380 + NodeID
 * PDO1TX                   400 + NodeID
 * PDO1RX                   480 + NodeID
 * PDO1TX                   500 + NodeID
 *  	
 * SDORX                    580 + NodeID
 * SDOTX                    600 + NodeID 
 * 
 * NMT_HB                   700 + NodeID 
 * 
 * 
 */


/*
 * Feeder CanOpen Supported Commands
 * 
 *  Communication object          COB-ID(s) hex     Comment
 * 
 * 1  NMT node control            000               To be implemented
 *    Global failsafe command     001             
 * 2  Sync                        080 + 0           To be implemented
 * 3  Emergency                   080 + NodeID      To be implemented
 * 4  TimeStamp                   100               To be implemented
 * 
 * 5. Transmit PDO 1              180 + NodeID      Feeder status report (length 1)
 *    Receive PDO 1               200 + NodeID      Feed Command (length 1)
 *    Transmit PDO 2              280 + NodeID      Keyboard command (length 1)
 *    Receive PDO 2               300 + NodeID      
 *    Transmit PDO 3              380 + NodeID      Slot Status report (length 1)    TBI
 *    Receive PDO 3               400 + NodeID      
 *    Transmit PDO 4              480 + NodeID
 *    Receive PDO 4               500 + NodeID
 *  
 * 6. Transmit SDO                580 + NodeID
 *    Receive SDO                 600 + NodeID 	
 * 
 * 7. NMT heartbeat               700 + NodeID      Reports operational, every 5 seconds
 * 
 */

/*
 * PNP CAN NodeId map
 * 
 * 0        All nodes / No node
 * 1        PNP_MB_V2 CPU
 * ...
 * 20       Slot with address 0
 * 21       Slot with address 1
 * 22       Slot with address 2
 * 23       Slot with address 3
 * 24       Slot with address 4
 * 25       Slot with address 5
 * 26       Slot with address 6
 * 27       Slot with address 7
 * 28       Slot with address 8
 * 29       Slot with address 9
 * 30       Slot with address 10
 * 31       Slot with address 11
 * 32       Slot with address 12
 * 33       Slot with address 13
 * 34       Slot with address 14
 * 35       Slot with address 15
 * 36       Slot with address 16
 * 37       Slot with address 17
 * 38       Slot with address 18
 * 
 * 
 */

// *****************************************************************************

//Variables

// *****************************************************************************

// *****************************************************************************

#endif /* CAN_FRAME_H */

/* *****************************************************************************
 End of File
 */
